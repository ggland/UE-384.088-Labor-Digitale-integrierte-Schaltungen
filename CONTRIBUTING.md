Es gibt mehrere Möglichkeiten wie du etwas Beitragen kannst.

# Ohne Git
Wenn du von Git und GitLab keine Ahnung hast, kannst du dennoch etwas beitragen. Du kannst alle Aufgaben erledigen, welche als text editiert werden können. Dazu muss in der TODO Liste stehen, dass diese Aufgabe auch ohne Git durchgeführt werden kann. Beachte bitte, dass du für jede Aufgabe, welche du ohne Git löst um EINEN Punkt weniger erhältst, als wenn du direkt mit Git arbeitest. Das hängt damit zusammen, dass andere deine Aufgaben einpflegen müssen.

1. Erstell eine Liste an Änderungen/ Korrekturen/ Verbesserungen, eine .tex Datei oder eine pdf von einem eingescannten Dokument.
1. Wenn möglich führe einen test Build durch um zu sehen, ob dein latex code auch wirklich funktioniert.
    
    ```
    $ make test
    ```
    Im /conf/out Ordner wird ein test.pdf erstellt.
    
    

## Über ein Issue
Erstelle ein Issue und kopiere dort die Datei hinein.

## Private Nachricht
Schicke im et-forum eine Private Nachricht an Painkilla mit deiner Lösung.

## Thread Posten
Poste im jeweiligen Thread deine Lösung.

## Telegram Gruppe vom OMN
Schicke deine Lösung an die Telegram Gruppe. [https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ](https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ)

# Mit Git und GitLab
Wenn du dich mit Git, GitLab und LaTeX auskennst, wähle bitte diesen Weg.

## Einmalige Initialisierung
Hier wird beschrieben, welche Schritte einmalig durchführen musst, um das Repo auf deinen PC zu bekommen.

1. Registriere und melde dich bei GitLab an.
1. Lade dir Git herunter.  [https://git-scm.com/download/win](https://git-scm.com/download/win)
1. Installiere Git, aktiviere im Installationsdialog "Git Credential Manager" und "Enable Symbolic Links"
1. Lade dir einen LaTeX Editor herunter. Empfohlen für Windows nutzer, TeXStudio [https://github.com/texstudio-org/texstudio/releases/download/2.12.14/texstudio-2.12.14-win-qt5.exe](https://github.com/texstudio-org/texstudio/releases/download/2.12.14/texstudio-2.12.14-win-qt5.exe)
1. Geh in GitLab auf die Lehrveranstaltung, zu der du etwas beitragen möchtest. Diese Git Repo heißt für jede Lehrveranstaltung UPSTREAM
1. Klick rechts oben auf das Feld "Fork" um einen Abkömmling zu erstellen. GitLab erstellt dir eine Kopie vom UPSTREAM Repo. Deine Kopie heißt jetzt ORIGIN.
1. Klick in deinem soeben erstellten Abkömmling auf das grüne Feld "Clone or Download" Symbol.
1. Das ist DEINE ORIGIN_URL, kopiere Sie.
1. Öffne deinen Explorer und erstelle einen Ordner für Uni.
1. Öffne diesen Ordner und rechtsklicke und klick auf Git Bash here.
1. Kopiere (Clone) deinen Fork auf deinen lokalen PC. Gib dazu in die Konsole folgende Befehle ohne dem $ Zeichen ein.  Die URL musst du durch deine kopierte ORIGIN_URL ersetzen.

    ```
    $ git clone ORIGIN_URL
    $ cd ./Type-LVA_Nummer-Name-der-LVA
    $ git submodule init conf
    $ git submodule update conf
    ```
1. Git lädt das open und conf Repo auf deinen PC herunter.
1. Wir fügen jetzt in den nächsten Schritten das UPSTREAM Projekt als link hinzu, damit du neue Inhalte erhalten kannst.
1. Geh dazu in GitLab auf die UPSTREAM Lehrveranstaltung, zu der du etwas beitragen möchtest.
1. Klicke auf das grüne Feld "Clone or Download".
1. Das ist die UPSTREAM_URL, kopiere Sie.
1. Füge das UPSTREAM Repo mit folgendem Befehl hinzu.

    ```
    $ git remote add -t master upstream UPSTREAM_URL
    ```
1. Überprüfe deine remote Repos. Es sollten hier 4 Zeilen erscheinen

    ```
    $ git remote -v
    origin   ORIGIN_URL (fetch)
    origin   ORIGIN_URL (push)
    upstream UPSTREAM_URL (fetch)
    upstream UPSTREAM_URL (push)
    ```
    
## Wiederkehrend
Hier wird beschrieben, welche Schritte notwendig sind damit du etwas beitragen kannst. Führe diese Schritte jedes mal in dieser Reinfolge aus.

1. Hole dir die neuesten Inhalte vom UPSTREAM Repo.

    ```
    $ git pull upstream master
    ```
1. Führe deine Änderungen durch. z.B. (neue LaTeX Dokumente, neue Source Code, neue PDFs, usw.)
1. Führe einen test Build durch um zu sehen, ob dein code auch wirklich funktioniert.
    
    ```
    $ make test
    ```
    Im /conf/out Ordner wird ein test.pdf erstellt.
1. Schau dir deine Änderungen mit

    ```
    $ git status
    ```
    an.
    Das kann dann so aussehen:

    ```
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
    (use "git add ..." to update what will be committed)
    (use "git checkout -- ..." to discard changes in working directory)

    modified: opn/exm/mdl/chp/3/3/relaxion.tex

    no changes added to commit (use "git add" and/or "git commit -a")
    ```
1. Füge die Datein, welche du hinzufügen möchtest mit 

    ```
    $ git add opn/exm/mdl/chp/3/3/relaxion.tex
    ```
    hinzu und schau dir mit
    
    ```
    $ git status
    ```
    an, ob git die Datei auch akzeptiert hat. Git hat die Datei akzeptiert, da sie unter dem Text "Changes to be commited" steht.

    ```
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
    (use "git reset HEAD ..." to unstage)

    modified: opn/exm/mdl/chp/3/3/relaxion.tex

    Changes not staged for commit:
    (use "git add ..." to update what will be committed)
    (use "git checkout -- ..." to discard changes in working directory)
    ```
1. Schreibe einen aussagekräftigen Kommentar zu deinen Änderungen mit:

    ```
    $ git commit -m"Dein aussagekräftiger Kommentar zu deinen Änderungen"
    ```
    Die Ausgabe kann so aussehen:
    
    ```
    [master 3475954] Deine Änderungen
    1 file changed, 2 insertions(+), 2 deletions(-)
    ```
1. Push diesen commit auf deinen Fork, also ORIGIN mit:

    ```
    $ git push origin master
    ```
1. Geh auf die GitLab Seite der UPSTREAM Lehrveranstaltung und erstelle einen Pull-Request, damit deine Änderungen in das UPSTREAM Repo übernommen werden und wieder für alle zur Verfügung steht.


# Ich bin für den closed Bereich freigeschalten. Wie bekomme ich die Inhalte?
Diesen Punkt BITTE NUR EINMAL ausführen. Damit wird dein closed Repo angelegt und heruntergeladen.

1. Geh in den Hauptordner in welchem sich die Lehrveranstaltung befindet für die du freigeschaltet wurdest.
1. Öffne die git konsole mit rechtsklick Git Bash here
1. führe folgende Befehle aus:

    ```
    $ git submodule init cld
    $ git submodule update cld
    ```
1. Git wird nach dem GitLab username und passwort fragen. Das muss dann hier eingegeben werden.
1. Füge das UPSTREAM Repo als Link hinzu.
    Git initialisiert die erste URL immer als origin, unabhängig davon um welches Projekt es sich handelt.
    Gib dazu folgenden Befehl ein, um die upstream URL zu sehen.
    
    ```
    $ cd ./cld
    $ git remote -v
    origin   UPSTREAM_URL_CLD (fetch)
    origin   UPSTREAM_URL_CLD (push)
    ```
    Kopiere oder tippe diese UPSTREAM_URL_CLD in folgendem Befehl ab.
    
    ```
    $ git remote add -t master upstream UPSTREAM_URL_CLD
    ```
1. Wenn du nun dir die remote Repos anzeigen lässt, sollte die UPSTREAM_URL_CLD vier mal vorkommen.

    ```
    $ git remote -v
    origin   UPSTREAM_URL_CLD (fetch)
    origin   UPSTREAM_URL_CLD (push)
    upstream UPSTREAM_URL_CLD (fetch)
    upstream UPSTREAM_URL_CLD (push)
    ```
1. Lies das README und TODO im closed Bereich. Es unterscheidet sich von den opn Dokumenten.
